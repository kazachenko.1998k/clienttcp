#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>

using namespace std;

string message;
char buf[1024];

string sign_in_up() {
    string result;
    string currentAnswer;
    cout << "Вы зарегестрированы? y/n\n";
    getline(cin, currentAnswer);
    if (strcmp(currentAnswer.c_str(), "y") == 0) {
        result += "signIn ";
        cout << "Введите логин\n";
        getline(cin, currentAnswer);
        result += currentAnswer;
    } else {
        result += "signUp ";
        cout << "Введите логин для новой учетной записи\n";
        getline(cin, currentAnswer);
        result += currentAnswer;
        result += " ";
    }
    return result;
}

string insert() {
    string result;
    string currentAnswer;
    result += "insert";
    cout << "Введите название нового продукта\n";
    getline(cin, currentAnswer);
    result += " ";
    result += currentAnswer;

    cout << "Введите стоимость нового продукта\n";
    getline(cin, currentAnswer);
    result += " ";
    result += currentAnswer;

    cout << "Введите количество товара на продажу\n";
    getline(cin, currentAnswer);
    result += " ";
    result += currentAnswer;
    return result;
}

string buy() {
    string result;
    string currentAnswer;
    result += "buy";
    cout << "Введите id товара для покупки\n";
    getline(cin, currentAnswer);
    result += " ";
    result += currentAnswer;

    cout << "Введите количество для покупки\n";
    getline(cin, currentAnswer);
    result += " ";
    result += currentAnswer;
    return result;
}

void Event(int port, int adr) {
    int sock;
    struct sockaddr_in addr{};

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port); // или любой другой порт...
    addr.sin_addr.s_addr = adr;
    if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("connect");
        exit(2);
    }
    read(sock, buf, 1024);
    cout << buf << "\n";
    bool isWork = true;
    send(sock, sign_in_up().c_str(), 1024, 0);
    if (read(sock, buf, 1024) <= 0) {
        std::cout << "Сервер отключился\n";
        isWork = false;
    }
    std::cout << buf << "\n";
    while (isWork) {
        std::getline(std::cin, message);
        if (strcmp(message.c_str(), std::string("insert").c_str()) == 0) {
            message = insert();
        }
        send(sock, message.c_str(), 1024, 0);
        if (read(sock, buf, 1024) <= 0) {
            std::cout << "Сервер отключился\n";
            break;
        }
        std::cout << buf << "\n";
        if (strcmp(message.c_str(), std::string("exit").c_str()) == 0) {
            isWork = false;
        }
    }
    shutdown(sock, 2);
    close(sock);
}

int main(int argc, char *argv[]) {
    Event(atoi(argv[1]), inet_addr(argv[2]));
    return 0;
}
